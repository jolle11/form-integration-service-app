import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-pokemon-finder',
    templateUrl: './pokemon-finder.component.html',
    styleUrls: ['./pokemon-finder.component.scss'],
})
export class PokemonFinderComponent implements OnInit {
    @Output() sendName = new EventEmitter<string>();
    public finderForm: FormGroup = new FormGroup({});
    constructor() {}

    ngOnInit(): void {
        this.finderForm = new FormGroup({
            name: new FormControl('', []),
        });
    }
    onSubmit(): void {
        this.sendName.emit(this.finderForm.value.name);
    }
}
