import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokemonRoutingModule } from './pokemon-routing.module';
import { PokemonComponent } from './pokemon.component';
import { PokemonFinderComponent } from './components/pokemon-finder/pokemon-finder.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PokemonService } from './pokemon.service';

@NgModule({
    declarations: [PokemonComponent, PokemonFinderComponent],
    imports: [CommonModule, PokemonRoutingModule, ReactiveFormsModule, HttpClientModule],
    providers: [PokemonService],
})
export class PokemonModule {}
