import { Component, OnInit } from '@angular/core';
import { ApiDataModel } from './models/pokemon.model';
import { PokemonService } from './pokemon.service';

@Component({
    selector: 'app-pokemon',
    templateUrl: './pokemon.component.html',
    styleUrls: ['./pokemon.component.scss'],
})
export class PokemonComponent implements OnInit {
    public pokemonFound: ApiDataModel | null = null;
    constructor(private pokemonService: PokemonService) {}

    ngOnInit(): void {}

    handlePokemon(pokemonName: string): void {
        this.getPokemon(pokemonName);
    }

    getPokemon(pokemonName: string) {
        this.pokemonService.getPokemonByName(pokemonName).subscribe((data: ApiDataModel) => {
            console.log(data);
            this.pokemonFound = data;
        });
    }
}
