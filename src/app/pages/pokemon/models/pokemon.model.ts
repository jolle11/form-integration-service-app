export interface ApiDataModel {
    id: number;
    name: string;
    sprites: SpritesModel;
}
export interface SpritesModel {
    front_default: string;
}
